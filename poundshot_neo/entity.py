#!/usr/bin/env python3

# Poundshot Neo - A Python 3 reimplementation of Poundshot
# Copyright (C) 2015 Robert Cochran
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import curses

class Entity():
    def __init__(self, x = -1, y = -1, rep = '!'):
        self.x = x
        self.y = y
        self.rep = ord(rep)

    def render(self, context):
        try:
            context.addch(self.y, self.x, self.rep)
        except curses.error:
            pass

    def isTouching(self, other):
        if not isinstance(other, Entity):
            raise TypeError("Comparison object is not an Entity")

        return (self.x == other.x) and (self.y == other.y)

class Player(Entity):
    def __init__(self, x = -1, y = -1):
        super().__init__(x, y, '#')

    def update(self, i):
        if i == 'a':
            if self.x > 0:
                self.x -= 1

        elif i == 'd':
            if self.x < (curses.COLS - 1):
                self.x += 1

        elif i == 'w':
            if self.y > 1:
                self.y -= 1

        elif i == 's':
            if self.y < (curses.LINES - 1):
                self.y += 1

class Bullet(Entity):
    def __init__(self):
        super().__init__(-1, -1, '+')
        self.direction = ""
        self.active = False

    def update(self, i, p):
        if not self.active and i in ('i', 'j', 'k', 'l'):
            self.x = p.x; self.y = p.y;
            self.active = True

            if i == 'i': self.direction = "up"; self.y -= 1
            elif i == 'k': self.direction = "down"; self.y += 1
            elif i == 'j': self.direction = "left"; self.x -= 1
            elif i == 'l': self.direction = "right"; self.x += 1

        else:
            if self.direction == "up":
                if self.y > 1:
                    self.y -= 1
                else:
                    self.active = False

            elif self.direction == "down":
                if self.y < (curses.LINES - 1):
                    self.y += 1
                else:
                    self.active = False

            elif self.direction == "left":
                if self.x > 1:
                    self.x -= 1
                else:
                    self.active = False

            elif self.direction == "right":
                if self.x < (curses.COLS - 1):
                    self.x += 1
                else:
                    self.active = False

    def render(self, context):
        if self.active:
            super().render(context)
