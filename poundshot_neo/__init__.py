#!/usr/bin/env python3

# Poundshot Neo - A Python 3 reimplementation of Poundshot
# Copyright (C) 2015 Robert Cochran
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# System imports
import curses

# Project imports
from . import entity

def main_loop(stdscr):
    player = entity.Player(5, 7)
    bullet = entity.Bullet()

    curses.use_default_colors()
    curses.curs_set(False)

    while True:
        curses.halfdelay(1)

        i = ''

        try:
            i = stdscr.getkey()
        except curses.error:
            # No big deal - continue onward
            pass

        if i == 'q':
            break

        # Logic

        player.update(i)

        bullet.update(i, player)

        # Rendering

        stdscr.clear()

        player.render(stdscr)

        bullet.render(stdscr)

        stdscr.refresh()

    curses.curs_set(False)

def run():
    curses.wrapper(main_loop)
